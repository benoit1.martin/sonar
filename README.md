# GitLab CI template for SonarQube

This project implements a generic GitLab CI template for running [SonarQube](https://www.sonarqube.org/) analysis.

SonarQube is a **Code Quality and Security tool** that helps you analyse your source code and detect quality issues or
security vulnerabilities as early as possible.

## Usage

In order to include this template in your project, add the following to your `gitlab-ci.yml`:

```yaml
include:
  - project: 'Orange-OpenSource/tbc/sonar'
    ref: '1.1.0'
    file: '/templates/gitlab-ci-sonar.yml'
```

## SonarQube analysis job

This job performs a SonarQube analysis of your code.

It is bound to the `test` stage, and uses the following variables:

| Name                     | description                     | default value |
| ------------------------ | ------------------------------- | ----------------------------- |
| `SONAR_SCANNER_IMAGE`    | The Docker image used to run [sonar-scanner](https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/) | `sonarsource/sonar-scanner-cli:latest` |
| `SONAR_URL`              | SonarQube server url            | _none_ (disabled) |
| :lock: `SONAR_AUTH_TOKEN`| SonarQube authentication [token](https://docs.sonarqube.org/latest/user-guide/user-token/) (depends on your authentication method) | _none_ |
| :lock: `SONAR_LOGIN`     | SonarQube login (depends on your authentication method)                | _none_ |
| :lock: `SONAR_PASSWORD`  | SonarQube password (depends on your authentication method)             | _none_ |
| `SONAR_BASE_ARGS`        | SonarQube [analysis arguments](https://docs.sonarqube.org/latest/analysis/analysis-parameters/) | `-Dsonar.host.url=${SONAR_URL} -Dsonar.projectKey=${CI_PROJECT_PATH_SLUG} -Dsonar.projectName=${CI_PROJECT_PATH} -Dsonar.projectBaseDir=. -Dsonar.links.homepage=${CI_PROJECT_URL} -Dsonar.links.ci=${CI_PROJECT_URL}/-/pipelines -Dsonar.links.issue=${CI_PROJECT_URL}/-/issues` |
| :lock: `SONAR_GITLAB_TOKEN` | GitLab [access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with `api` scope. When set, activates the [Sonar GitLab plugin](https://github.com/gabrie-allaigre/sonar-gitlab-plugin/#plugins-properties) integration. | _none_ |
| `SONAR_BRANCH_ANALYSIS_DISABLED` | Set to disable automatic [Pull Request Analysis](https://docs.sonarqube.org/latest/analysis/pull-request/) and [Branch Analysis](https://docs.sonarqube.org/latest/branches/overview/)  | _none_ (enabled) |
| `SONAR_GITLAB_ARGS`      | Extra arguments to use with [Sonar GitLab plugin](https://github.com/gabrie-allaigre/sonar-gitlab-plugin/#plugins-properties) | `-Dsonar.gitlab.url=${CI_SERVER_URL} -Dsonar.gitlab.user_token=${SONAR_GITLAB_TOKEN} -Dsonar.gitlab.project_id=${CI_PROJECT_ID} -Dsonar.gitlab.commit_sha=${CI_COMMIT_SHA} -Dsonar.gitlab.ref_name=${CI_COMMIT_REF_NAME}` |
| `SONAR_AUTO_ON_DEV_DISABLED` | When set, SonarQube analysis becomes **manual** on development branches (automatic otherwise) | _none_ |

#### Automatic Branch Analysis & Pull Request Analysis

By default, this template tries to auto-detect and use [Pull Request Analysis](https://docs.sonarqube.org/latest/analysis/pull-request/) or [Branch Analysis](https://docs.sonarqube.org/latest/branches/overview/) (depending on the context).

Those is a great SonarQube features but it assumes one of the following conditions:

* you are using a [Developer Edition](https://www.sonarqube.org/developer-edition/) version,
* or you are using Community Edition with an opensource plugin emulating those features, such as [sonarqube-community-branch-plugin](https://github.com/mc1arke/sonarqube-community-branch-plugin).

If you're not in one of those cases, then you shall disable this feature by setting `SONAR_BRANCH_ANALYSIS_DISABLED`.

If you leave the feature enabled, if `SONAR_AUTH_TOKEN` is provided, the template will try to autodetect (using GitLab APIs) an opened merge request matching the current branch:

* If one is found, a SonarQube [Pull Request Analysis](https://docs.sonarqube.org/latest/analysis/pull-request/) will be made.
* Otherwise, a simple [Branch Analysis](https://docs.sonarqube.org/latest/branches/overview/) is performed on the current branch.

### About Sonar GitLab plugin

The [Sonar GitLab plugin](https://github.com/gabrie-allaigre/sonar-gitlab-plugin) uses the GitLab APIs to inline comments
into your commits directly in GitLab for each new anomaly.

As explained above, this template automatically enables the Sonar GitLab plugin if `SONAR_GITLAB_TOKEN` is set.
It will then simply append the `SONAR_GITLAB_ARGS` (overridable) to the SonarQube analysis arguments.

Comments added to GitLab will appear as owned by the user associated to the GitLab [access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).

### How should I configure other SonarQube arguments ?

The simplest and recommended way to configure other SonarQube [analysis arguments](https://docs.sonarqube.org/latest/analysis/analysis-parameters/)
(and even [language specific args](https://docs.sonarqube.org/latest/analysis/languages/overview/)) is to use a
`sonar-project.properties` file at the root of your repository.
